package com.idyllic.newsarticleapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.idyllic.newsarticleapp.fragment.ArticleFragment;
import com.idyllic.newsarticleapp.fragment.BlankFragment;
import com.idyllic.newsarticleapp.fragment.NotificationFragment;

public class MainActivity extends AppCompatActivity {

    private FrameLayout mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    ArticleFragment articleFragment = new ArticleFragment();
                    replaceFragment(articleFragment,null);

                    return true;
                case R.id.navigation_dashboard:
                    BlankFragment blankFragment = new BlankFragment();
                    replaceFragment(blankFragment,null);
                    return true;
                case R.id.navigation_notifications:
                    NotificationFragment notificationFragment = new NotificationFragment();
                    replaceFragment(notificationFragment,null);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (FrameLayout) findViewById(R.id.frm_container);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        ArticleFragment articleFragment = new ArticleFragment();
        replaceFragment(articleFragment,null);

    }

    public void replaceFragment(Fragment fragment, String _stack) {
        String backStateName = _stack;

        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frm_container, fragment, _stack);
        transaction.addToBackStack(backStateName);
        transaction.commit();

    }
}
