package com.idyllic.newsarticleapp.network;


import com.idyllic.newsarticleapp.models.Article;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Swapnil on 22/1/19.
 */

public interface NetworkInterface {

    @GET("everything?q=bitcoin&from=2018-12-22&sortBy=publishedAt&apiKey=83fb1d10b24c42fea47ea7f9f3fb5394")
    Observable<Article> getArticle();

}
