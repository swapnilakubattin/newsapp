package com.idyllic.newsarticleapp.models;

import java.util.List;

public class Article {



    public String status;
    public int totalResults;
    public List<ArticleModel> articles;

    public Article(String status, int totalResults, List<ArticleModel> articles) {
        this.status = status;
        this.totalResults = totalResults;
        this.articles = articles;
    }

    public String getStatus() {
        return status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public List<ArticleModel> getArticles() {
        return articles;
    }
}
