package com.idyllic.newsarticleapp.adapter;

/**
 * Created by Swapnil
 */

import android.content.Context;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.idyllic.newsarticleapp.R;
import com.idyllic.newsarticleapp.fragment.ArticleFragment;
import com.idyllic.newsarticleapp.models.Article;
import com.idyllic.newsarticleapp.models.ArticleModel;

import java.util.ArrayList;
import java.util.List;

public class ArticleRecyclerViewAdapter extends RecyclerView.Adapter<ArticleRecyclerViewAdapter.ItemRowHolder> {

    private ArrayList<ArticleModel> dataList;
    private FragmentActivity context;


    public ArticleRecyclerViewAdapter(FragmentActivity context, ArrayList<ArticleModel> dataList) {
        this.dataList = dataList;
        this.context = context;

    }



    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_row, parent,false);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final ItemRowHolder itemRowHolder, final int position) {

        final ArticleModel articleModel = dataList.get(position);

        itemRowHolder.txtTitle.setText(articleModel.getTitle());
        itemRowHolder.txtAuthorName.setText(articleModel.getAuthor());
        
        Glide.with(context)
                .load(articleModel.getUrlToImage())
                .into(itemRowHolder.imgArticle);

    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

         TextView txtTitle;
         TextView txtAuthorName;
         ImageView imgArticle;



        public ItemRowHolder(View view) {
            super(view);

            txtTitle = (TextView) view.findViewById(R.id.txt_title);
            txtAuthorName = (TextView) view.findViewById(R.id.txt_title);
            imgArticle = (ImageView) view.findViewById(R.id.img_article);


        }

    }

    public void setData(ArrayList<ArticleModel> commentList) {
        dataList = commentList;
        notifyDataSetChanged();
    }

}