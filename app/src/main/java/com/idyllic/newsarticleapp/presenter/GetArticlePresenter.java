package com.idyllic.newsarticleapp.presenter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;


import com.idyllic.newsarticleapp.models.Article;
import com.idyllic.newsarticleapp.network.NetworkClient;
import com.idyllic.newsarticleapp.network.NetworkInterface;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 *
 */

public class GetArticlePresenter extends GetArticleContract.Presenter {
    private static final String TAG = "GetArticlePresenter";



    @Override
    public void performGetArticle(final FragmentActivity activity) {

        view.showProgress("Get Article...");

        getObservable().subscribeWith(getObserver());


    }

    public Observable<Article> getObservable(){
        return NetworkClient.getRetrofit().create(NetworkInterface.class)
                .getArticle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public DisposableObserver<Article> getObserver(){
        return new DisposableObserver<Article>() {

            @Override
            public void onNext(@NonNull Article movieResponse) {
                Log.d(TAG,"OnNext"+movieResponse.getTotalResults());
                view.onGetArticleSuccess(movieResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.d(TAG,"Error"+e);
                e.printStackTrace();
                view.onGetArticleFailed("Error fetching Movie Data");
            }

            @Override
            public void onComplete() {
                Log.d(TAG,"Completed");
                view.dismissProgress();
            }
        };
    }

}
