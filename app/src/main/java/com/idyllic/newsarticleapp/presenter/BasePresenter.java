package com.idyllic.newsarticleapp.presenter;




import com.idyllic.newsarticleapp.BaseView;

import retrofit2.Retrofit;

/**
 * Created by Swapnil on 07/26/18.
 */

public abstract class BasePresenter<V extends BaseView> {

    protected V view;
    protected Retrofit retrofit;


/*     protected DatabaseHelper databaseHelper;
    protected ProgressDialog progressDialog;
    protected DBConnection dbConnection;
    protected InputStream inputStream;
    protected SharedPreferences sharedPreferences;
    protected Common_Function_For_All common_Function_For_All;
*/

    public void attachView(V view)
    {
        this.view = view;
    }


    public void attachApiClient(Retrofit retrofit) {
        this.retrofit = retrofit;
    }


   /* public void attachDatabase(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    public void attachProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public void attachDBConnection(DBConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void attachpreferences(SharedPreferences preferences) {
        this.sharedPreferences = preferences;
    }

    public Common_Function_For_All getCommon_Function_For_All() {
        return common_Function_For_All;
    }

    public void setCommon_Function_For_All(Common_Function_For_All common_Function_For_All) {
        this.common_Function_For_All = common_Function_For_All;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }*/
}
