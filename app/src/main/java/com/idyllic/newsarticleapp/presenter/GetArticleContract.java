package com.idyllic.newsarticleapp.presenter;

import android.support.v4.app.FragmentActivity;

import com.idyllic.newsarticleapp.BaseView;
import com.idyllic.newsarticleapp.models.Article;


/**
 *
 */

public class GetArticleContract {


    public interface View extends BaseView {

        void onGetArticleSuccess(Article responseData);

        void onGetArticleFailed(String msg);

        void showProgress(String msg);

        void dismissProgress();

        void showEmptyError(String message);

    }


    public static abstract class Presenter extends BasePresenter<View> {

        public abstract void performGetArticle(FragmentActivity activity);


    }

}
