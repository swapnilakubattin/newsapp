package com.idyllic.newsarticleapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idyllic.newsarticleapp.R;
import com.idyllic.newsarticleapp.adapter.ArticleRecyclerViewAdapter;
import com.idyllic.newsarticleapp.models.Article;
import com.idyllic.newsarticleapp.models.ArticleModel;
import com.idyllic.newsarticleapp.presenter.GetArticleContract;
import com.idyllic.newsarticleapp.presenter.GetArticlePresenter;

import java.util.ArrayList;

/**
 *
 */
public class ArticleFragment extends Fragment implements GetArticleContract.View{


    private View mView;
    private RecyclerView mArticlesRecyclerView;
    private GetArticlePresenter getArticlePresenter;
    private ArticleRecyclerViewAdapter adapter;
    private ArrayList<ArticleModel> articleList;


    public ArticleFragment() {
        // Required empty public constructor
    }

    public static ArticleFragment newInstance(String param1, String param2) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_article, container, false);

        initView(mView);
        initPresenter();
        return mView;
    }

    private void initPresenter() {

        getArticlePresenter = new GetArticlePresenter();
        getArticlePresenter.attachView(this);
        getArticlePresenter.performGetArticle(getActivity());

    }

    private void initView(View mView) {

        mArticlesRecyclerView = (RecyclerView) mView.findViewById(R.id.rv_article);
        mArticlesRecyclerView.setHasFixedSize(true);
        articleList = new ArrayList<ArticleModel>();



    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onGetArticleSuccess(Article responseData) {

        articleList = (ArrayList<ArticleModel>) responseData.getArticles();
        adapter = new ArticleRecyclerViewAdapter(getActivity(), articleList);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mArticlesRecyclerView.setLayoutManager(llm);
        mArticlesRecyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();

    }

    @Override
    public void onGetArticleFailed(String msg) {

    }

    @Override
    public void showProgress(String msg) {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void showEmptyError(String message) {

    }
}
